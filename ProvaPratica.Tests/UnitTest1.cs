using NUnit.Framework;
using ProvaPratica;

namespace ProvaPratica.Tests
{
    public class Tests
    {

        [Test]
        public void TesteCPF()
        {
            string digito = "11833384067";
            bool result = true;
            Assert.AreEqual(result, Validador.CPF(digito));
            
        }
        [Test]
        public void TesteCNPJ()
        {
            string digito = "23.324.238/0001-78";
            bool result = true;
            Assert.AreEqual(result, Validador.CNPJ(digito));
            
        }
    }
}